let selectedAccountType = ''; // Either 'speedrun', 'main', or both

function toggleSearch(type) {
    const speedrunToggle = document.getElementById('speedrunToggle');
    const mainToggle = document.getElementById('mainToggle');

    if (type === 'speedrun') {
        speedrunToggle.parentNode.classList.add('active');
        mainToggle.parentNode.classList.remove('active');
        selectedAccountType = 'speedrun';
    } else if (type === 'main') {
        mainToggle.parentNode.classList.add('active');
        speedrunToggle.parentNode.classList.remove('active');
        selectedAccountType = 'main';
    }

    // Ensure at least one is selected
    if (!speedrunToggle.checked && !mainToggle.checked) {
        // Default to 'speedrun' if neither is selected
        selectedAccountType = 'speedrun';
    }
}

async function fetchGames() {
    const username = document.getElementById('username').value;

    if (!username) {
        alert('Please enter a Chess.com username.');
        return;
    }

    let accountsToCheck = [];

    // Determine which JSON file(s) to load based on the selected toggle
    if (selectedAccountType === 'speedrun') {
        accountsToCheck.push(fetch('verifiedAccounts.json').then(res => res.json()));
    } else if (selectedAccountType === 'main') {
        accountsToCheck.push(fetch('verifiedMainAccounts.json').then(res => res.json()));
    } else if (selectedAccountType === 'both') {
        accountsToCheck.push(fetch('verifiedAccounts.json').then(res => res.json()));
        accountsToCheck.push(fetch('verifiedMainAccounts.json').then(res => res.json()));
    }

    // Process the fetched data
    Promise.all(accountsToCheck)
        .then(data => {
            const [verifiedAccounts, verifiedMainAccounts] = data;
            // Now you can search through both verifiedAccounts and verifiedMainAccounts
            console.log(verifiedAccounts, verifiedMainAccounts);
            // Continue your logic here to search games based on the retrieved accounts
        })
        .catch(error => console.error('Error fetching accounts:', error));
}


function displayCard(opponentInfo) {
    const resultsDiv = document.getElementById('results');
    const card = document.createElement('div');
    card.className = 'card';
    const gamesList = opponentInfo.games.map(game => {
        return `<li class="game-info"><a href="${game.url}" target="_blank">Game Link</a><span>${game.result}</span><span>${game.timeControl}</span></li>`;
    }).join('');

    card.innerHTML = `
        <div class="info">
            <img src="${opponentInfo.data.avatar || 'https://www.chess.com/bundles/web/images/user-image.007dad08.svg'}" alt="${opponentInfo.data.username}" width="50" height="50">
            <h2>${opponentInfo.data.username}</h2>
            <p>${opponentInfo.description}</p>
            <button class="show-games" onclick="toggleGames(this)">Show games</button>
            <ul class="games-list">
                <li class="game-info game-header"><span>Game Link</span><span>Result</span><span>Time Control</span></li>
                ${gamesList}
            </ul>
        </div>
    `;
    resultsDiv.appendChild(card);
}

function toggleGames(element) {
    const gamesList = element.nextElementSibling;
    if (gamesList.style.display === 'none' || gamesList.style.display === '') {
        gamesList.style.display = 'block';
        element.textContent = 'Hide games';
    } else {
        gamesList.style.display = 'none';
        element.textContent = 'Show games';
    }
}

